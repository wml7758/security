package com.example.demo.mybatis.mapper;

import com.example.demo.services.MyUserDetails;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface UserMapper {

    @Select("select * from users1 where username=#{username}")
    MyUserDetails findByUserName(@Param("username") String username);
}
