package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * 创建自定义表，存储权限
 * CREATE TABLE `users1` (
 *   `id` bigint(20) NOT NULL AUTO_INCREMENT,
 *   `username` varchar(50) NOT NULL,
 *   `password` varchar(60) DEFAULT NULL,
 *   `enable` tinyint(4) NOT NULL DEFAULT '1',
 *   `roles` text,
 *   PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 * insert into users1 (username, password, roles) values('admin', '123', 'ROLE_ADMIN, ROLE_USER');
 * insert into users1 (username, password, roles) values('user', '123', 'ROLE_USER');
 */
@EnableWebSecurity
public class WebSecurityConfig6 extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()//对应<intercept-url>
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                //.antMatchers("/api/admin/**").hasAnyAuthority("")
                .antMatchers("/api/user/**").hasRole("USER")

                .antMatchers("/api/app/**").permitAll()
                .anyRequest().authenticated()
                .and()
            .logout()
                .logoutUrl("/api/logout")//无需在Controller中定义，默认为/logout
                .logoutSuccessHandler(new LogoutSuccessHandler() {
                    @Override
                    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                        System.out.println("退出成功");
                    }
                })
                .permitAll()
                .and()
            .httpBasic()
                .and()

        ;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }


}
