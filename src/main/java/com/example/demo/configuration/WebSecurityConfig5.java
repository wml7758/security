package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;

/**
 * CREATE TABLE `users` (
 *   `username` varchar(50) NOT NULL,
 *   `password` varchar(50) NOT NULL,
 *   `enabled` tinyint(1) DEFAULT NULL,
 *   PRIMARY KEY (`username`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 *
 * CREATE TABLE `authorities` (
 *   `username` varchar(50) NOT NULL,
 *   `authority` varchar(50) NOT NULL
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 */
//@EnableWebSecurity
public class WebSecurityConfig5 extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()//对应<intercept-url>
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                .antMatchers("/api/user/**").hasRole("USER")
                .antMatchers("/api/app/**").permitAll()
                .anyRequest().authenticated()
                .and()
             .httpBasic()
                .and()
             .csrf().disable()
        ;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }


    @Autowired
    private DataSource dataSource;

    @Bean
    public UserDetailsService userDetailsService(){
        JdbcUserDetailsManager manager = new JdbcUserDetailsManager();
        manager.setDataSource(dataSource);
        if(!manager.userExists("admin")){
            manager.createUser(User.withUsername("admin").password("123").roles("ADMIN").build());
        }
        if(!manager.userExists("user")) {
            manager.createUser(User.withUsername("user").password("123").roles("USER").build());
        }
        return manager;
    }
}
