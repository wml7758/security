package com.example.demo.configuration;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()//对应<intercept-url>
                //.antMatchers("/*.html").permitAll()
                //.regexMatchers().permitAll()
                .anyRequest().authenticated()
                .and()
             .formLogin()//对应<form-login>
                .loginPage("/myLogin.html")//同时会自动注册一个/myLogin.html的路由用于接收form的action请求。
                .permitAll()//登录页不限访问
                .and()
             //.httpBasic().and()//对应<http-basic>
             .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("")
                .and()
             .csrf().disable()//对应<csrf>
                ;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
    }
}
