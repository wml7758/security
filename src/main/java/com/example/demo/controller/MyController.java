package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyController {

    @GetMapping("/admin")
    public String admin(){
        return "hello, admin";
    }

    @GetMapping("/user")
    public String user(){
        return "hello, user";
    }

    @GetMapping("/app")
    public String app(){
        return "hello, app";
    }


    @GetMapping("/login_success")
    public String loginSuccess(){
        return "login success";
    }

    @GetMapping("/logout_success")
    public String logout(){
        return "logout_success";
    }
}
